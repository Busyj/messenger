'''
Пример работы с flask.
Реализация месенджера клиент- сервер.
Модуль отсылающий сообщения в БД сервера.
'''
import requests

# запрос к серверу
response = requests.get(
    'http://127.0.0.1:5000/status',
    json={}
)

# статус ответа от сервера
print(response.status_code)
print(response.json())