'''
Пример работы с flask.
Реализация месенджера клиент- сервер.
Модуль сервера.
'''
from datetime import datetime
import time

from flask import Flask, Response, request

app = Flask(__name__)

# база сообщений
db = [
    {'text':'Привет, страдалец!','author':'Rob','time':time.time()},
    {'text':'Привет, дружище!','author':'Max','time':time.time()},
    {'text':'Привет, всем!','author':'Tina','time':time.time()}
]

# первая страница, приветствие
@app.route("/")
def hello():
    return "Hello world!<br><a href='/status'>Статус</a>"

# страница проверки статуса работы сервера
@app.route("/status")
def status():
    server_time = datetime.now()
    return {
        'status': True,
        'name': 'Messenger',
        'time': time.time(),
        'time2': str(server_time),
        'time3': server_time.isoformat(),
        'time4': server_time.strftime('%Y-%m-%d %H:%M')
    }

# посылаем новое сообщение
@app.route("/send_msg", methods=['POST'])
def send_message(text, author):
    data = request.json
    if not isinstance(data,dict):
        return Response('error: not json', 400)

    text = data['text']
    author = data['author']

    # проверка, являются ли текстом
    if isinstance(text,str) and isinstance(author,str):
        db.append({
            'text': text,
            'author': author,
            'time':time.time()
        })
        return Response('ok')
    else:
        return Response('error: wrong format',400)

# получаем сообщения с сервера
@app.route("/get_msg")
def get_messages():
    return {'messages':db}


app.run()